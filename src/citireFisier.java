import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class citireFisier {
	
	private String fileName;

	public citireFisier(String path) {
		fileName = path;
	}
	
	public void run() {
		try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
			stream.forEach(System.out::println);
		} catch (IOException e) {
	         e.printStackTrace();
	    }
	}
	
}